-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 28, 2018 at 08:23 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reservasi-vila`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `room_id` int(10) UNSIGNED NOT NULL,
  `price_id` int(10) UNSIGNED NOT NULL,
  `check_in` date NOT NULL,
  `check_out` date NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `room_id`, `price_id`, `check_in`, `check_out`, `status`, `created_at`, `updated_at`, `customer_id`) VALUES
(1, 1, 1, '2018-07-24', '2018-07-27', 'Check Out', '2018-07-28 09:35:21', '2018-07-28 09:40:24', 1),
(2, 2, 2, '2018-07-14', '2018-07-15', 'Belum Transfer', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 2),
(3, 3, 3, '2018-07-24', '2018-07-26', 'Belum Transfer', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 3),
(4, 4, 4, '2018-07-22', '2018-07-24', 'Belum Transfer', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 4),
(5, 5, 5, '2017-07-22', '2017-07-24', 'Belum Transfer', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 4),
(6, 6, 6, '2017-07-22', '2017-07-24', 'Belum Transfer', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 4),
(7, 7, 7, '2017-07-22', '2017-07-24', 'Belum Transfer', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 4),
(8, 8, 8, '2017-07-22', '2017-07-24', 'Belum Transfer', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 4),
(9, 9, 9, '2017-07-22', '2017-07-24', 'Belum Transfer', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 4),
(10, 10, 10, '2017-07-22', '2017-07-24', 'Belum Transfer', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 4);

-- --------------------------------------------------------

--
-- Table structure for table `check_in`
--

CREATE TABLE `check_in` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `check_in` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `check_in`
--

INSERT INTO `check_in` (`id`, `booking_id`, `check_in`, `note`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-07-28 09:36:33', NULL, '2018-07-28 09:36:33', '2018-07-28 09:36:33');

-- --------------------------------------------------------

--
-- Table structure for table `check_outes`
--

CREATE TABLE `check_outes` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `check_out` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `check_outes`
--

INSERT INTO `check_outes` (`id`, `booking_id`, `check_out`, `note`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-07-28 09:40:24', NULL, '2018-07-28 09:40:24', '2018-07-28 09:40:24');

-- --------------------------------------------------------

--
-- Table structure for table `confirmations`
--

CREATE TABLE `confirmations` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_rek` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `atas_nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `phone_number`, `no_rek`, `bank`, `created_at`, `updated_at`, `atas_nama`) VALUES
(1, 'Tinny', 'tinny@gmail.com', '847383747', '12443223', 'BCA', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 'Tinny'),
(2, 'Denny', 'denny@gmail.com', '847383741', '12443222', 'BCA', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 'Denny'),
(3, 'Donny', 'donny@gmail.com', '847383743', '12443227', 'BRI', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 'Donny'),
(4, 'Winny', 'winny@gmail.com', '847383742', '12443229', 'BNI', '2018-07-28 09:35:21', '2018-07-28 09:35:21', 'Winny');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_06_12_114935_laratrust_setup_tables', 1),
(4, '2018_07_09_102833_create_rooms_table', 1),
(5, '2018_07_09_104557_create_prices_table', 1),
(6, '2018_07_09_112214_create_bookings_table', 1),
(7, '2018_07_09_115027_create_check_in_table', 1),
(8, '2018_07_09_115205_create_check_outes_table', 1),
(9, '2018_07_09_115237_create_profiles_table', 1),
(10, '2018_07_11_022024_create_customers_table', 1),
(11, '2018_07_11_142722_add_customer_id_in_bookings', 1),
(12, '2018_07_11_174527_update_field_rooms', 1),
(13, '2018_07_28_032507_add_atas_nama', 1),
(14, '2018_07_28_094856_create_confirmations_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `room_id` int(10) UNSIGNED NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`id`, `room_id`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 1000000, '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(2, 2, 2000000, '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(3, 3, 1000000, '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(4, 4, 2000000, '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(5, 5, 3000000, '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(6, 6, 1000000, '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(7, 7, 2000000, '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(8, 8, 1000000, '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(9, 9, 750000, '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(10, 10, 750000, '2018-07-28 09:35:20', '2018-07-28 09:35:20');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `company` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `company`, `address`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'Villa Jaya Sakti', 'Jl. Pelabuhan, Rancabuaya Kec Caringin - Garut', 63748362, '2018-07-28 09:35:20', '2018-07-28 09:35:20');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', NULL, '2018-07-28 09:35:18', '2018-07-28 09:35:18'),
(2, 'member', 'Member', NULL, '2018-07-28 09:35:18', '2018-07-28 09:35:18');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `room` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capacity` int(11) NOT NULL,
  `service` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room`, `capacity`, `service`, `created_at`, `updated_at`) VALUES
(1, 'R001', 2, 'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel', '2018-07-28 09:35:19', '2018-07-28 09:35:19'),
(2, 'R002', 4, 'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel', '2018-07-28 09:35:19', '2018-07-28 09:35:19'),
(3, 'R003', 2, 'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel', '2018-07-28 09:35:19', '2018-07-28 09:35:19'),
(4, 'R004', 4, 'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel', '2018-07-28 09:35:19', '2018-07-28 09:35:19'),
(5, 'R005', 6, 'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel', '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(6, 'R006', 2, 'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel', '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(7, 'R007', 4, 'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel', '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(8, 'R008', 2, 'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel', '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(9, 'R009', 1, 'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel', '2018-07-28 09:35:20', '2018-07-28 09:35:20'),
(10, 'R010', 1, 'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel', '2018-07-28 09:35:20', '2018-07-28 09:35:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin Larapus', 'admin@gmail.com', '$2y$10$.qf.G09u7.93Uw.YUWq1i.Oui.MZV1cOc4szvkEuAOrn/tgzGrsuO', 'shbFrFuxjykIgPXCfsRwXDaRZqlI3qU6CY4AdvBapqHLD1HZCXm8UqPJUjUb', '2018-07-28 09:35:19', '2018-07-28 09:35:19'),
(2, 'Sample Member', 'member@gmail.com', '$2y$10$qIwxaAgQVjromyGXmR4rfOfYmPLLYkN1Fxgs0jOXlBPeoqd009LV2', NULL, '2018-07-28 09:35:19', '2018-07-28 09:35:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings_room_id_foreign` (`room_id`),
  ADD KEY `bookings_price_id_foreign` (`price_id`),
  ADD KEY `bookings_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `check_in`
--
ALTER TABLE `check_in`
  ADD PRIMARY KEY (`id`),
  ADD KEY `check_in_booking_id_foreign` (`booking_id`);

--
-- Indexes for table `check_outes`
--
ALTER TABLE `check_outes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `check_outes_booking_id_foreign` (`booking_id`);

--
-- Indexes for table `confirmations`
--
ALTER TABLE `confirmations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `confirmations_booking_id_foreign` (`booking_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prices_room_id_foreign` (`room_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `check_in`
--
ALTER TABLE `check_in`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `check_outes`
--
ALTER TABLE `check_outes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `confirmations`
--
ALTER TABLE `confirmations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bookings_price_id_foreign` FOREIGN KEY (`price_id`) REFERENCES `prices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bookings_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `check_in`
--
ALTER TABLE `check_in`
  ADD CONSTRAINT `check_in_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `check_outes`
--
ALTER TABLE `check_outes`
  ADD CONSTRAINT `check_outes_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `confirmations`
--
ALTER TABLE `confirmations`
  ADD CONSTRAINT `confirmations_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `prices`
--
ALTER TABLE `prices`
  ADD CONSTRAINT `prices_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

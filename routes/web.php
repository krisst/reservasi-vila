<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/about', function () {
    return view('about');
});
Route::get('/facilities', function () {
    return view('facilities');
});
Route::get('/galery', function () {
    return view('galery');
});
Route::get('/testimony', function () {
    return view('testimony');
});
Route::get('/confirmation/form', function () {
    return view('confirmations._form');
});
Route::post('/search/room', 'BookingsController@searchRoom');
Route::post('/booking/form', 'BookingsController@bookingForm');
Route::post('/booking/process', 'BookingsController@bookingAction');

Auth::routes();
Route::resource('confirmationpayments', 'ConfirmationPaymentsController');

Route::get('/home', 'HomeController@index')->name('home');	
Route::group(['prefix'=>'admin', 'middleware'=>['auth','role:admin']], function () {
	Route::resource('rooms', 'RoomsController');
	Route::resource('users', 'UsersController');
	Route::resource('list_booking', 'ListBookingsController');
	Route::resource('guests', 'GuestsController');
	Route::resource('companies', 'CompaniesController');
	Route::resource('confirmations', 'ConfirmationsController');
	Route::get('settings/change-profile', 'SettingsController@changeProfile');
	Route::post('settings/change-profile', 'SettingsController@changeProfileAct');
	Route::get('settings/change-password', 'SettingsController@editPassword');
	Route::post('settings/change-password', 'SettingsController@updatePassword');
});

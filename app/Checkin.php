<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkin extends Model
{
    //
    protected $table='check_in';

    protected $fillable = [
        'booking_id',
        'check_in'
    ];
    
    public function booking(){
        return $this->belongsTo('App\Booking');
    }
}

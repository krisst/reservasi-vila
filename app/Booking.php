<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
    protected $table='bookings';

    public function room(){
        return $this->belongsTo('App\Room');
    }

    public function prices(){
        return $this->belongsTo('App\Price');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }

    public function confirmation(){
        return $this->hasOne('App\Confirmation');
    }

    public function checkin(){
        return $this->hasOne('App\Checkin');
    }

    public function checkout(){
        return $this->hasOne('App\Checkout');
    }

}

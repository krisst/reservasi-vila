<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model
{
    //
    protected $table='confirmations';

    protected $fillable = [
        'booking_id',
        'image',
    ];

    public function booking(){
        return $this->belongsTo('App\Booking');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table="customers";

    protected $fillable = [
        'name',
        'email',
        'phone_number',
        'no_rek',
        'bank',
        'atas_nama',
    ];

    public function booking(){
        return $this->hasMany('App\Customer');
    }

}

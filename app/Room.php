<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //
    protected $table='rooms';

    protected $fillable = [
        'room',
        'capacity',
        'service',
    ];
    
    public function prices(){
        return $this->hasOne('App\Price');
    }

    public function booking(){
        return $this->hasMany('App\Booking');
    }
}

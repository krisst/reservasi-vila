<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Room;
use App\Price;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        //
        if ($request->ajax()) {
            $prices = Price::with('room');
            return Datatables::of($prices)
                ->addColumn('action', function($price){
                return view('datatable._action', [
                    'model' => $price,
                    'form_url' => route('rooms.destroy', $price->id),
                    'edit_url' => route('rooms.edit', $price->id),
                    'detail_url' => route('rooms.show', $price->id),
                    'confirm_message' => 'Yakin mau menghapus ' . $price->nama . '?'
            ]);
            })->make(true);
        }
        $html = $htmlBuilder
            ->addColumn(['data' => 'room.room', 'name'=>'room.room', 'title'=>'Room'])
            ->addColumn(['data' => 'room.capacity', 'name'=>'room.capacity', 'title'=>'Capacity'])
            ->addColumn(['data' => 'price', 'name'=>'price', 'title'=>'Price'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'Action', 'orderable'=>false, 'searchable'=>false]);
        return view('rooms.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('rooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'room' => 'required|string|unique:rooms',
            'capacity' => 'required|max:11',
            'price' => 'required|max:11',
            'service' => 'required',
        ]);

        $room = new Room;
        $room->room = $request->room;
        $room->capacity = $request->capacity;
        $room->service = $request->service;
        $room->save();

        $price = new Price;
        $price->room_id = $room->id;
        $price->price = $request->price;
        $price->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $room->room"
        ]);
        return redirect()->route('rooms.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $price = Price::with('room')->where('id', $id)->first();
        return view('rooms.show')->with(compact('price'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $price = Price::with('room')->where('id', $id)->first();
        return view('rooms.edit')->with(compact('price'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $Request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        /*$room = Price::findOrFail($id);*/
        $price = Price::with('room')->where('id', $id)->first();
        $this->validate($request, [
            'room' => 'required|string|max:15|unique:rooms,room,'.$price->id,
            'capacity' => 'required|max:11',
            'price' => 'required|max:11',
            'service' => 'required',
        ]);

        $price->price = $request->price;
        $price->save();

        $room = Room::where('id',$id)->first();
        $room->room = $request->room;
        $room->capacity = $request->capacity;;
        $room->service = $request->service;
        $room->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil Mengubah $request->room"
        ]);
        return redirect()->route('rooms.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $room = Room::findOrFail($id);
        $room->delete();

        $price = Price::where('id', $id);
        $price->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Ruaangan dihapus"
        ]);
        return redirect()->route('rooms.index');
    }
}

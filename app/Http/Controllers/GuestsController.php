<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use App\Booking;
use App\Checkin;
use App\Checkout;

class GuestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        //
        if ($request->ajax()) {
            $bookings = Booking::with('room','prices','customer','checkin','checkout');
            return Datatables::of($bookings)
                ->addColumn('action', function($booking){
                return view('datatable._action2', [
                    'model' => $booking,
                    'action_url' => route('guests.show', $booking->id)
            ]);
            })->make(true);
        }
        $html = $htmlBuilder
            ->addColumn(['data' => 'id', 'name'=>'id', 'title'=>'No Transaksi'])
            ->addColumn(['data' => 'room.room', 'name'=>'room.room', 'title'=>'Room'])
            ->addColumn(['data' => 'customer.name', 'name'=>'customer.name', 'title'=>'Customer'])
            ->addColumn(['data' => 'checkin.check_in', 'name'=>'checkin.check_in', 'title'=>'Check In'])
            ->addColumn(['data' => 'checkout.check_out', 'name'=>'checkout.check_out', 'title'=>'Check Out'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'Action', 'orderable'=>false, 'searchable'=>false]);
        return view('guests.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $bookings = Booking::with('room','prices','customer','checkin','checkout')->where('id',$id)->first();
        $konversi_check_in = new Carbon($bookings->check_in);
        $konversi_check_out = new Carbon($bookings->check_out);
        $lama = $konversi_check_in->diffInDays($konversi_check_out);
        $total = $bookings->room->prices->price * $lama;
        /*dd($total);exit();*/
        return view('guests.show')->with(compact('bookings','lama','total'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Booking;
use App\Room;
use App\Customer;
use App\Price;
use PDF;
use Carbon\Carbon;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Messages\MailMessage;
use App\Mail\Emailconfirmation;

class BookingsController extends Controller
{
    //
    function searchRoom(Request $request){
    	$check_in = $request->check_in;
		$check_out = $request->check_out;
		$capacity = $request->number;

        $check_ins = date('Y-m-d', strtotime($check_in));
        $check_outes = date('Y-m-d', strtotime($check_out));

		$search_room = Room::with('booking')/*where('check_in', '=', $check_in)
			->where('check_out', '<=', $check_out)*/
            ->where('capacity', '=', $capacity)
            // ->whereNotBetween('booking.check_in', [$check_ins, $check_outes])
            ->whereHas("booking", function ($query) use ($check_ins, $check_outes) {
				// $query->whereRaw("check_in <= '$check_in' && check_out >= '$check_out' ");
                // $query->whereNotBetween('check_in', [$check_ins, $check_outes]);
                // $query->WhereNotBetween('check_out', [$check_ins, $check_outes]);
				// $query->Where('check_in', '=', '');
                $query->whereRaw("check_in NOT BETWEEN '$check_ins' AND '$check_outes'");
				$query->whereRaw("check_out NOT BETWEEN '$check_ins' AND '$check_outes'");
                $query->orWhereRaw("check_in != $check_ins && check_in != $check_outes");
                $query->orWhereRaw("check_out != $check_ins && check_out != $check_outes");

            })
            /*->whereHas("room",function($q) use($capacity){
                    $q->whereRaw("capacity <= '$capacity'");
                    #$q->orwhere('tgl_booking','>=','$hari');
            })*/
            ->get();
            // dd($search_room);exit();

        /*$loop = [];
        foreach ($search_room as $search_room) {
        	# code...
        	$loop[] = $search_room->room_id;
        }
        $search_room = Booking::with('room')->whereNotIn('id',$loop)->get();*/
        /*dd($search_room);exit();*/
        return view('bookings.search-room',compact('search_room','check_in','check_out'));
    }

    function bookingForm(Request $request){
		$room_id = $request->room_id;
    	$check_in = $request->check_in;
		$check_out = $request->check_out;
		/*dd($room_id);exit();*/
        return view('bookings.form-booking',compact('room_id','check_in','check_out'));
    }

    function bookingAction(Request $request){
		$this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:50',
            'no_rek' => 'required|max:20',
            'no_rek' => 'required|max:20',
            'bank' => 'required',
            'atas_nama' => 'required',
        ]);

        $room_id = $request->room_id;
        $check_in = $request->check_in;
        $check_out = $request->check_out;
        $name = $request->name;
        $email = $request->email;
        $phone_number = $request->phone_number;

        $konversi_check_in = new Carbon($check_in);
        $konversi_check_out = new Carbon($check_out);
        $lama = $konversi_check_in->diffInDays($konversi_check_out);
		
		$customer = new Customer;
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone_number = $request->phone_number;
        $customer->no_rek = $request->no_rek;
        $customer->bank = $request->bank;
        $customer->save();
		
		$booking = new Booking;
        $booking->room_id = $request->room_id;
        $booking->price_id = $request->room_id;
        $booking->check_in = $request->check_in;
        $booking->check_out = $request->check_out;
        $booking->status = "Belum Transfer";
        $booking->customer_id=$customer->id;
        $booking->save();

        $no_trans =$booking->id;

        $room = Room::select('room')->where('id',$room_id)->value('room');
        $price = Price::select('price')->where('room_id',$room_id)->value('price');
        $total = $price * $lama ;

        $booking = $this;
        Mail::send('bookings.send-email', compact('room','no_trans','check_in','check_out','name','email','phone_number','lama','price','total','room_id'), function ($m) use ($request) {
            $m->to($request->email)->subject('Silahkan melakukan transfer pembayaran');
        });
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Terima kasih, silahkan Cek email anda untuk melakukan transfer Pembayaran & konfirmasi pembayaran"
        ]);
        return redirect('/');

		/*dd($room_id);exit();*/
        /*return view('bookings.form-booking',compact('room_id','check_in','check_out'));*/
    }
}

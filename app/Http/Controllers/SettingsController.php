<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Role_user;
use App\User;
use App\Role;
use Hash;


class SettingsController extends Controller
{
    //
    public function changeProfile(Request $request){
    	$user_id = Auth::user()->id;
        $users = Role_user::with('user','role')->where('user_id',$user_id)->first();
        return view('users.change-profile')->with(compact('users'));
    }

    public function changeProfileAct(Request $request){
    	$user_id = Auth::user()->id;
        $users = Role_user::with('user','role')->where('user_id',$user_id)->first();

        $this->validate($request, [
            'name' => 'required|max:50',
            'email' => 'required|string|max:50|unique:rooms,room,'.$users->id,
        ]);

        $user = User::where('id',$user_id)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

		Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $request->name"
        ]);
        return redirect()->route('users.index');
    }

    public function editPassword()
	{
		return view('users.edit-password');
	}

	public function updatePassword(Request $request)
	{
		$user = Auth::user();
		
		$this->validate($request, [
			'password' => 'required',
			'new_password' => 'required|confirmed|min:6',
		]);

		if (!(Hash::check($request->get('password'), Auth::user()->password))) {
            Session::flash("flash_notification", [
				"level"=>"danger",
				"message"=>"Password lama tidak cocok"
			]);
            return redirect()->back();
        }

		$user->password = bcrypt($request->get('new_password'));
		$user->save();
		Session::flash("flash_notification", [
			"level"=>"success",
			"message"=>"Password berhasil diubah"
		]);
        return redirect()->route('users.index');
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Booking;
use App\Confirmation;
use Carbon\Carbon;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;

class ConfirmationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        //
        if ($request->ajax()) {
            $confirmations = Confirmation::with('booking','booking.room','booking.prices','booking.customer');
            return Datatables::of($confirmations)
                ->addColumn('action', function($confirmation){
                return view('datatable._action1', [
                    'model' => $confirmation,
                    'detail_url' => route('confirmations.show', $confirmation->id),
                    'confirm_message' => 'Yakin mau menghapus ' . $confirmation->nama . '?'
            ]);
            })->make(true);
        }
        $html = $htmlBuilder
            ->addColumn(['data' => 'booking.id', 'name'=>'booking.id', 'title'=>'No Transaksi'])
            ->addColumn(['data' => 'booking.room.room', 'name'=>'booking.room.room', 'title'=>'Room'])
            ->addColumn(['data' => 'booking.customer.name', 'name'=>'booking.customer.customer', 'title'=>'Customer'])
            ->addColumn(['data' => 'booking.check_in', 'name'=>'booking.check_in', 'title'=>'Check In'])
            ->addColumn(['data' => 'booking.check_out', 'name'=>'booking.check_out', 'title'=>'Check Out'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'Action', 'orderable'=>false, 'searchable'=>false]);
        return view('confirmations.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $confirmations = Confirmation::with('booking','booking.room','booking.room.prices','booking.prices','booking.customer')->where('booking_id',$id)->first();
        $konversi_check_in = new Carbon($confirmations->booking->check_in);
        $konversi_check_out = new Carbon($confirmations->booking->check_out);
        $lama = $konversi_check_in->diffInDays($konversi_check_out);
        $total = $confirmations->booking->room->prices->price * $lama;
        /*dd($total);exit();*/
        return view('confirmations.show')->with(compact('confirmations','lama','total'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function confirmationForm()
    {
        //
        return view('confirmations._form');
    }

    public function confirmationPayment(Request $request)
    {
        //

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Booking;
use App\Confirmation;

class ConfirmationPaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'booking_id' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $booking_id = $request->booking_id;
        $image = $request->image;

        $cek = DB::table('bookings')->select('id')->where('id',$booking_id);
        if($cek->count() < 1){
            Session::flash("flash_notification", [
                "level"=>"danger",
                "message"=>"Nomor Transaksi tidak ditemukan"
            ]);
            return redirect()->route('bukti_trans.index');
        }

        $cek1 = DB::table('confirmations')->select('booking_id')->where('booking_id',$booking_id);
        if($cek1->count() > 1){
            Session::flash("flash_notification", [
                "level"=>"danger",
                "message"=>"Anda sudah upload bukti transfer"
            ]);
            return redirect()->route('bukti_trans.index');
        }

        $confirmation = Confirmation::create($request->except('image'));

        if ($request->hasFile('image')) {
            $uploaded_cover = $request->file('image');
                // mengambil extension file
                $extension = $uploaded_cover->getClientOriginalExtension();
                // membuat nama file random berikut extension
                $filename = md5(time()) . '.' . $extension;
                // menyimpan cover ke folder public/img
                $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
                $uploaded_cover->move($destinationPath, $filename);
                $confirmation->image = $filename;
                $confirmation->save();
        }
        
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Bukti Telah diupload"
        ]);
        return redirect('/confirmation/form');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

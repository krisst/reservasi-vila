<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use App\Booking;
use App\Checkin;
use App\Checkout;

class ListBookingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        //
        if ($request->ajax()) {
            $bookings = Booking::with('room','prices','customer');
            return Datatables::of($bookings)
                ->addColumn('action', function($booking){
                return view('datatable._action2', [
                    'model' => $booking,
                    'action_url' => route('list_booking.show', $booking->id)
            ]);
            })->make(true);
        }
        $html = $htmlBuilder
            ->addColumn(['data' => 'id', 'name'=>'id', 'title'=>'No Transaksi'])
            ->addColumn(['data' => 'room.room', 'name'=>'room.room', 'title'=>'Room'])
            ->addColumn(['data' => 'customer.name', 'name'=>'customer.name', 'title'=>'Customer'])
            ->addColumn(['data' => 'check_in', 'name'=>'check_in', 'title'=>'Check In'])
            ->addColumn(['data' => 'check_out', 'name'=>'check_out', 'title'=>'Check Out'])
            ->addColumn(['data' => 'status', 'name'=>'status', 'title'=>'Status'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'Action', 'orderable'=>false, 'searchable'=>false]);
        return view('bookings.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $bookings = Booking::with('room','prices','customer','room.prices')->where('id',$id)->first();
        $konversi_check_in = new Carbon($bookings->check_in);
        $konversi_check_out = new Carbon($bookings->check_out);
        $lama = $konversi_check_in->diffInDays($konversi_check_out);
        $total = $bookings->room->prices->price * $lama;
        /*dd($total);exit();*/
        return view('bookings.show')->with(compact('bookings','lama','total'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        date_default_timezone_set('Asia/Jakarta');
        $this->validate($request, [
            'status' => 'required',
        ]);

        $now = date('Y-m-d H:i:s');
        if($request->status == "Check In"){
            $check_in = new Checkin;
            $check_in->booking_id = $id;
            $check_in->check_in = $now;
            $check_in->save();
        }

        if($request->status == "Check Out"){
            $check_out = new Checkout;
            $check_out->booking_id = $id;
            $check_out->check_out = $now;
            $check_out->save();
        }
        /*$tgl = "2015-01-01";
        $konversi_tgl = date($tgl);*/
        if($request->status == "Dibatalkan"){
            $room = Booking::where('id',$id)->first();
            $room->check_in = null;
            $room->check_out = null;
            $room->status = $request->status;
            $room->save();

            Session::flash("flash_notification", [
                "level"=>"success",
                "message"=>"Status Sudah diubah"
            ]);
            return redirect()->route('list_booking.index');
        }

        $room = Booking::where('id',$id)->first();
        $room->status = $request->status;
        $room->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Status Sudah diubah"
        ]);
        return redirect()->route('list_booking.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

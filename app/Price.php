<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    //
    protected $table='prices';

    protected $fillable = [
        'room_id',
        'price',
    ];

    public function room(){
        return $this->belongsTo('App\Room');
    }

    public function booking(){
        return $this->hasMany('App\Booking');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    //
    protected $table='check_outes';

    protected $fillable = [
        'booking_id',
        'check_out'
    ];
    
    public function booking(){
        return $this->belongsTo('App\Booking');
    }
}

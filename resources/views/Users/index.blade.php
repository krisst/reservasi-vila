@extends('layouts.app')
@section('content')
	    <section class="content-header">
	      <h1>
	        User
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
	        <li class="active">Index</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      	<div class="panel panel-warning">
				<div class="panel-heading">
					<h2 class="panel-title">Data User</h2>
				</div>
				<div class="panel-body">
					<p> <a class="btn btn-primary" href="{{ url('/admin/settings/change-profile') }}">Change Profile</a> <a class="btn btn-success" href="{{ url('/admin/settings/change-password') }}">Change Password</a></p>
					<Table class="table table-striped table-hover table-bordered">
						<tr>
        					<td width="20%">ID</td>
							<td>{{ $users->user->id }}</td>
    					</tr>
    					<tr>
        					<td>Username</td>
							<td>{{ $users->user->name }}</td>
    					</tr>
    					<tr>
        					<td>Email</td>
							<td>{{ $users->user->email }}</td>
    					</tr>
    					<tr>
        					<td>Role</td>
							<td>{{ $users->role->display_name }}</td>
    					</tr>
					</Table>	
				</div>
			</div>
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->
@endsection
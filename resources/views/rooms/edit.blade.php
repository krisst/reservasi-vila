@extends('layouts.app')
@section('content')
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        Room
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="{{ route('rooms.index') }}"><i class="fa fa-columns"></i> Room</a></li>
	        <li class="active">Update</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      	<div class="panel panel-warning">
				<div class="panel-heading">
					<h2 class="panel-title">Update Data</h2>
				</div>
				<div class="panel-body">
					{!! Form::model($price, ['url' => route('rooms.update', $price->id),'method' => 'put', 'files'=>'true', 'class'=>'form-horizontal']) !!}
							@include('rooms._formedit')
						{!! Form::close() !!}
				</div>
			</div>
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->
@endsection
@extends('layouts.app')
@section('content')
	<!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        Room
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="{{ route('rooms.index') }}"><i class="fa fa-columns"></i> Room</a></li>
	        <li class="active">Detail</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      	<div class="panel panel-warning">
				<div class="panel-heading">
					<h2 class="panel-title">Detail</h2>
				</div>
				<div class="panel-body">
					<Table class="table table-striped table-hover table-bordered">
						<tr>
        					<td width="20%">ID</td>
							<td>{{ $price->id }}</td>
    					</tr>
    					<tr>
        					<td>Room</td>
							<td>{{ $price->room->room }}</td>
    					</tr>
    					<tr>
        					<td>Capacity</td>
							<td>{{ $price->room->capacity }}</td>
    					</tr>
    					<tr>
        					<td>Price</td>
							<td>Rp. {{ number_format($price->price, 2) }}</td>
    					</tr>
    					<tr>
        					<td>Service</td>
							<td>{{ $price->room->service }}</td>
    					</tr>
					</Table>
				</div>
			</div>
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->
@endsection
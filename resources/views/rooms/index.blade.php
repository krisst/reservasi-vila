@extends('layouts.app')
@section('content')
	<!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        Room
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="{{ route('rooms.index') }}"><i class="fa fa-columns"></i> Room</a></li>
	        <li class="active">Index</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      	<div class="panel panel-warning">
				<div class="panel-heading">
					<h2 class="panel-title">Data Room</h2>
				</div>
				<div class="panel-body">
					<p> <a class="btn btn-primary" href="{{ url('/admin/rooms/create') }}">Tambah</a></p>
					{!! $html->table(['class'=>'table table-striped table-bordered table-hover']) !!}
				</div>
			</div>
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->
@endsection

@section('scripts')
	{!! $html->scripts() !!}
@endsection
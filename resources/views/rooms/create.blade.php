@extends('layouts.app')
@section('content')
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        Room
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="{{ route('rooms.index') }}"><i class="fa fa-columns"></i> Room</a></li>
	        <li class="active">Create</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      	<div class="panel panel-warning">
				<div class="panel-heading">
					<h2 class="panel-title">Create Data</h2>
				</div>
				<div class="panel-body">
					{!! Form::open(['url' => route('rooms.store'), 'method' => 'post', 'files'=>'true', 'class'=>'form-horizontal']) !!}
						@include('rooms._form')
					{!! Form::close() !!}
				</div>
			</div>
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->
@endsection
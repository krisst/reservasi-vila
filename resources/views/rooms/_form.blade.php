<div class="form-group{{ $errors->has('room') ? ' has-error' : '' }}">
	{!! Form::label('room', 'Room', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::text('room', null, ['class'=>'form-control']) !!}
		{!! $errors->first('room', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('capacity') ? ' has-error' : '' }}">
	{!! Form::label('capacity', 'Capacity', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::number('capacity', null, ['class'=>'form-control']) !!}
		{!! $errors->first('capacity', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('service') ? ' has-error' : '' }}">
	{!! Form::label('service', 'Service', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::textarea('service', null, ['class'=>'form-control']) !!}
		{!! $errors->first('service', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
	{!! Form::label('price', 'Price', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::number('price', null, ['class'=>'form-control']) !!}
		{!! $errors->first('price', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group">
	<div class="col-md-2 col-md-offset-2">
		<button type="submit" class="btn btn-primary">
			<i class="fa fa-btn fa-file"></i> Simpan
		</button>
	</div>
</div>
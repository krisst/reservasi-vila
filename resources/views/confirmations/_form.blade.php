@extends('layouts.home')

@section('content')
    

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('/about') }}">About</a></li>
                        <li><a href="{{ url('/facilities') }}">Facilities</a></li>
                        <li class=""><a href="{{ url('/galery') }}">Galery</a></li>
                        <li class=""><a href="{{ url('/testimony') }}">Testimony</a></li>
                        <li class="active"><a href="{{ url('/confirmation/form') }}">Confirmation</a></li>
                    </ul>
                    
                </div><!-- /.navbar-collapse -->    
                
            </nav>
        </div>
    </div>
    <!-- blog -->
    <div class="blog">
        <div class="container">
        @include('layouts._flash')
            <h2 class="animated wow zoomIn" data-wow-delay=".5s">Confirmation Payment</h2>
            <div class="blog-grids">
                {!! Form::open(['url' => route('confirmationpayments.store'), 'method' => 'post', 'files'=>'true', 'class'=>'form-horizontal']) !!}
					<div class="form-group{{ $errors->has('booking_id') ? ' has-error' : '' }}">
						{!! Form::label('booking_id', 'No. Transaksi', ['class'=>'col-md-2 control-label']) !!}
						<div class="col-md-4">
							{!! Form::text('booking_id',null, ['class'=>'form-control']) !!}
							{!! $errors->first('booking_id', '<p class="help-block">:message</p>') !!}
						</div>
					</div>
					<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
						{!! Form::label('image', 'Bukti Transfer', ['class'=>'col-md-2 control-label']) !!}
						<div class="col-md-4">
							{!! Form::file('image') !!}
							{!! $errors->first('image', '<p class="help-block">:message</p>') !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-2 col-md-offset-2">
							<button type="submit" class="btn btn-primary">
								Upload
							</button>
						</div>
					</div>
				{!! Form::close() !!}
            </div>
        </div>
    </div>
<!-- //blog -->
@endsection
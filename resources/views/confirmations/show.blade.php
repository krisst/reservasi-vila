@extends('layouts.app')
@section('content')
	<!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        Confirmation
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="{{ route('rooms.index') }}"><i class="fa fa-phone"></i> Confirmation</a></li>
	        <li class="active">Detail</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      	<div class="panel panel-warning">
				<div class="panel-heading">
					<h2 class="panel-title">Detail</h2>
				</div>
				<div class="panel-body">
					<Table class="table table-striped table-hover table-bordered">
						<tr>
        					<td width="20%">No. Transaction</td>
							<td>{{ $confirmations->booking_id }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Customer</td>
							<td>{{ $confirmations->booking->customer->name }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Room</td>
							<td>{{ $confirmations->booking->room->room }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Capacity</td>
							<td>{{ $confirmations->booking->room->capacity }} /people</td>
    					</tr>
    					<tr>
        					<td width="20%">Check In</td>
							<td>{{ $confirmations->booking->check_in }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Check Out</td>
							<td>{{ $confirmations->booking->check_out }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Price</td>
							<td>Rp. {{ number_format($confirmations->booking->room->prices->price,2) }} /day</td>
    					</tr>
    					<tr>
        					<td width="20%">Length Of Stay</td>
							<td>{{ $lama }} day</td>
    					</tr>
    					<tr>
        					<td width="20%">Total</td>
							<td>Rp. {{ number_format($confirmations->booking->room->prices->price,2) }} </td>
    					</tr>
    					<tr>
        					<td width="20%">Evidence Of Transfer</td>
							<td>{!! Html::image(asset('img/'.$confirmations->image), null, ['class'=>'img-rounded img-responsive']) !!} </td>
    					</tr>
					</Table>
				</div>
			</div>
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->
@endsection
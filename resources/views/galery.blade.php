@extends('layouts.home')

@section('content')
    

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('/about') }}">About</a></li>
                        
                        <li><a href="{{ url('/facilities') }}">Facilities</a></li>
                        <li class="active"><a href="{{ url('/galery') }}">Galery</a></li>
                        <li class=""><a href="{{ url('/testimony') }}">Testimony</a></li>
                        <li class=""><a href="{{ url('/confirmation/form') }}">Confirmation</a></li>
                    </ul>
                    
                </div><!-- /.navbar-collapse -->    
                
            </nav>
        </div>
    </div>
    <!-- blog -->
    <div class="blog">
        <div class="container">
            <h2 class="animated wow zoomIn" data-wow-delay=".5s">Galery</h2>
            <div class="blog-grids">
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/1.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/2.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/3.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/4.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/5.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/6.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/7.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/8.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                                                            <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/9.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/10.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/11.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/12.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/13.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/14.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/15.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/16.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                                        <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/17.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/18.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/19.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/20.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/21.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/22.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/23.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/24.jpg" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>

        </div>
    </div>
<!-- //blog -->
@endsection
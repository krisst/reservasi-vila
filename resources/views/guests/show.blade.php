@extends('layouts.app')
@section('content')
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        Guest
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="{{ route('rooms.index') }}"><i class="fa fa-book"></i> Guest</a></li>
	        <li class="active">Detail</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      	<div class="panel panel-warning">
				<div class="panel-heading">
					<h2 class="panel-title">Details</h2>
				</div>
				<div class="panel-body">
					<Table class="table table-striped table-hover table-bordered">
						<tr>
        					<td width="20%">No. Transaction</td>
							<td>{{ $bookings->id }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Customer</td>
							<td>{{ $bookings->customer->name }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Room</td>
							<td>{{ $bookings->room->room }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Capacity</td>
							<td>{{ $bookings->room->capacity }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Check In</td>
        					@if($bookings->checkin->check_in)
								<td>{{ $bookings->checkin->check_in }}</td>
							@else
								<td>-</td>
							@endif
    					</tr>
    					<tr>
        					<td width="20%">Check Out</td>
							@if($bookings->checkout->check_out)
								<td>{{ $bookings->checkout->check_out }}</td>
							@else
								<td>-</td>
							@endif
    					</tr>
    					<tr>
        					<td width="20%">Price</td>
							<td>Rp. {{ number_format($bookings->room->prices->price,2) }} /day</td>
    					</tr>
    					<tr>
        					<td width="20%">Length Of Stay</td>
							<td>{{ $lama }} day</td>
    					</tr>
    					<tr>
        					<td width="20%">Total</td>
							<td>Rp. {{ number_format($total,2) }}</td>
    					</tr>
					</Table>
				</div>
			</div>
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->
@endsection
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Villa Jaya Sakti</title>

        <!-- Fonts -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> -->

        <!-- Styles -->
        <!-- //for-mobile-apps -->
        <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link rel="stylesheet" href="{{ asset('/css/jquery-ui.css') }}" />
        <link href="{{ asset('/css/animate.min.css') }}" rel="stylesheet"> 
        <!-- //animation-effect -->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
        
            <div class="header-top">
                <div class="container">
                    <div class="header-top-left animated wow slideInLeft" data-wow-delay=".5s">
                        <p>Welcome to Villa Jaya Sakti, Kenyamanan anda adalah prioritas kami</p>
                    </div>
                    <div class="header-top-left1 animated wow slideInLeft" data-wow-delay=".7s">
                        <h1>Contact Us 082240947873 <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></h1>
                    </div>
                    <div class="header-top-right">
                        <div id="loginContainer login">
                            <a href="#" id="loginButton"><span>Login</span></a>
                            <div id="loginBox">                
                                <form id="loginForm" method="post" action="{{ route('login') }}">
                                    {{ csrf_field() }}
                                    <fieldset id="body">
                                        <fieldset>
                                            <label for="email">Email Address</label>
                                            <input type="text" name="email" id="email">
                                        </fieldset>
                                        <fieldset>
                                            <label for="password">Password</label>
                                            <input type="password" name="password" id="password">
                                        </fieldset>
                                        <input type="submit" id="login" value="Sign in">
                                        <label for="checkbox"><input type="checkbox" id="checkbox"> <i>Remember me</i></label>
                                    </fieldset>
                                    <span><a href="#">Forgot your password?</a></span>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            
            </div>
        
    </div>
    <div class="header-nav">
        <div class="container">
            <nav class="navbar navbar-default">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                    <div class="logo animated wow slideInLeft" data-wow-delay=".5s">
                        <a class="navbar-brand" href="{{ url('/') }}">Jaya Sakti <span>Villa</span></a>
                    </div>
                </div>

        @yield('content')
        <!-- footer -->
    <div class="footer-copy">
        <div class="container"> 
            <div class="footer-left " data-wow-delay=".5s">
                <p>© 2018 Villa Jaya Sakti. All rights reserved</p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
<!-- //footer -->
        <!-- js -->
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
        <script src="{{ asset('/js/wow.min.js') }}"></script>
        <script>
            new WOW().init();
        </script>
        <script src="{{ asset('/js/jquery-1.11.1.min.js') }}"></script>
        <script src="{{ asset('/js/jquery-ui.js') }}"></script>
        <script>
            $(function() {
                $( "#datepicker,#datepicker1" ).datepicker();
            });
        </script>
        <!-- //js -->
        <!-- login-pop-up -->
        <script src="{{ asset('/js/menu_jquery.js') }}"></script>
        <!-- //login-pop-up -->
        <!-- animation-effect -->
        <script src="{{ asset('/js/bootstrap.js') }}"></script>
    </body>
</html>
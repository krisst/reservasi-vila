@extends('layouts.home')

@section('content')
    

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('/about') }}">About</a></li>
                        <li><a href="{{ url('/facilities') }}">Facilities</a></li>
                        <li class=""><a href="{{ url('/galery') }}">Galery</a></li>
                        <li class="active"><a href="{{ url('/testimony') }}">Testimony</a></li>
                        <li class=""><a href="{{ url('/confirmation/form') }}">Confirmation</a></li>
                    </ul>
                    
                </div><!-- /.navbar-collapse -->    
                
            </nav>
        </div>
    </div>
    <!-- blog -->
    <div class="blog">
        <div class="container">
            <h2 class="animated wow zoomIn" data-wow-delay=".5s">Testimony</h2>
            <div class="blog-grids">
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/a.png" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/b.png" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/c.png" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/d.png" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <a href="#"><img src="images/c.png" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                    </div>
                    <div class="blog-grid1 animated wow slideInUp" data-wow-delay=".6s">
                        <a href="#"><img src="images/d.png" alt=" " class="img-responsive" /></a>
                        <div class="blog-grid1-info">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-grid">
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
<!-- //blog -->
@endsection
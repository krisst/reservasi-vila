@extends('layouts.home')

@section('content')
    

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('/about') }}">About</a></li>
                        
                        <li><a href="{{ url('/facilities') }}">Facilities</a></li>
                        <li class=""><a href="{{ url('/galery') }}">Galery</a></li>
                        <li class="active"><a href="{{ url('/testimony') }}">Testimony</a></li>
                        <li class="active"><a href="{{ url('/confirmation/form') }}">Confirmation</a></li>
                    </ul>
                    
                    <div class="search">
                        <form>
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span><input type="email" class="text" value="Enter Your Text..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Your Text...';}" required="">
                        </form>
                    </div>
                </div><!-- /.navbar-collapse -->    
                
            </nav>
        </div>
    </div>
    <!-- blog -->
    <div class="blog">
        <div class="container">
            <h2 class="animated wow zoomIn" data-wow-delay=".5s">Form Booking</h2>
            <div class="blog-grids">
                {!! Form::open(['url' => url('/booking/process'), 'method' => 'post', 'files'=>'true', 'class'=>'form-horizontal']) !!}
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						{!! Form::label('name', 'Nama', ['class'=>'col-md-2 control-label']) !!}
						<div class="col-md-4">
							{!! Form::text('name', null, ['class'=>'form-control']) !!}
							{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
						</div>
					</div>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						{!! Form::label('email', 'Email', ['class'=>'col-md-2 control-label']) !!}
						<div class="col-md-4">
							{!! Form::email('email', null, ['class'=>'form-control']) !!}
							{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
						</div>
					</div>

					<div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
						{!! Form::label('phone_number', 'No. Telp/HP', ['class'=>'col-md-2 control-label']) !!}
						<div class="col-md-4">
							{!! Form::text('phone_number', null, ['class'=>'form-control']) !!}
							{!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
						</div>
					</div>

					<div class="form-group{{ $errors->has('no_rek') ? ' has-error' : '' }}">
						{!! Form::label('no_rek', 'No Rekening', ['class'=>'col-md-2 control-label']) !!}
						<div class="col-md-4">
							{!! Form::text('no_rek', null, ['class'=>'form-control']) !!}
							{!! $errors->first('no_rek', '<p class="help-block">:message</p>') !!}
						</div>
					</div>

					<div class="form-group{{ $errors->has('bank') ? ' has-error' : '' }}">
						{!! Form::label('bank', 'Bank', ['class'=>'col-md-2 control-label']) !!}
						<div class="col-md-4">
							<select name="bank" class="form-control">
								<option value="">--Pilih--</option>
								<option value="BCA">BCA</option>
								<option value="BNI">BNI</option>
								<option value="BRI">BRI</option>
								<option value="Danamon">Danamon</option>
								<option value="Mandiri">Mandiri</option>
							</select>
							{!! $errors->first('bank', '<p class="help-block">:message</p>') !!}
						</div>
					</div>

					<div class="form-group{{ $errors->has('atas_nama') ? ' has-error' : '' }}">
						{!! Form::label('atas_nama', 'Atas Nama', ['class'=>'col-md-2 control-label']) !!}
						<div class="col-md-4">
							{!! Form::text('atas_nama', null, ['class'=>'form-control']) !!}
							{!! $errors->first('atas_nama', '<p class="help-block">:message</p>') !!}
						</div>
					</div>

					<input type="text" name="room_id" value="{{ $room_id }}" hidden="hidden">
                                    <input type="date" name="check_in" value="{{ $check_in }}" hidden="hidden">
                                    <input type="date" name="check_out" value="{{ $check_out }}" hidden="hidden">
					<div class="form-group">
						<div class="col-md-2 col-md-offset-2">
							<button type="submit" class="btn btn-primary">
								<i class="fa fa-btn fa-file"></i> Booking
							</button>
						</div>
					</div>
				{!! Form::close() !!}
            </div>
        </div>
    </div>
<!-- //blog -->
@endsection
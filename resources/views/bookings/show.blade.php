@extends('layouts.app')
@section('content')
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        Form Booking
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="{{ route('rooms.index') }}"><i class="fa fa-book"></i> Booking</a></li>
	        <li class="active">Update</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      	<div class="panel panel-warning">
				<div class="panel-heading">
					<h2 class="panel-title">Update Status Booking</h2>
				</div>
				<div class="panel-body">
					<Table class="table table-striped table-hover table-bordered">
						<tr>
        					<td width="20%">No. Transaction</td>
							<td>{{ $bookings->id }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Customer</td>
							<td>{{ $bookings->customer->name }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Room</td>
							<td>{{ $bookings->room->room }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Capacity</td>
							<td>{{ $bookings->room->capacity }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Check In</td>
							<td>{{ $bookings->check_in }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Check Out</td>
							<td>{{ $bookings->check_out }}</td>
    					</tr>
    					<tr>
        					<td width="20%">Price</td>
							<td>Rp. {{ number_format($bookings->room->prices->price,2) }} /day</td>
    					</tr>
    					<tr>
        					<td width="20%">Length Of Stay</td>
							<td>{{ $lama }} day</td>
    					</tr>
    					<tr>
        					<td width="20%">Total</td>
							<td>Rp. {{ number_format($total,2) }}</td>
    					</tr>
					</Table>
					
					{!! Form::model($bookings, ['url' => route('list_booking.update', $bookings->id),'method' => 'put', 'files'=>'true', 'class'=>'form-horizontal']) !!}
						<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
							{!! Form::label('status', 'Status', ['class'=>'col-md-2 control-label']) !!}
							<div class="col-md-4">
							<select name="status" class="form-control" required="required">
					            <option value="{{ $bookings->status }}">{{ $bookings->status }}</option>
					            <option value="Belum Transfer">Belum Transfer</option>
					            <option value="Booking">Booking</option>
					            <option value="Check In">Check In</option> 
					            <option value="Check Out">Check Out</option>                                               
					            <option value="Dibatalkan">Dibatalkan</option>                                               
						    </select>
						    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
						    </div>
						</div>

						<div class="form-group">
							<div class="col-md-2 col-md-offset-2">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-file"></i> Simpan
								</button>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->
@endsection
@extends('layouts.home')

@section('content')
    

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('/about') }}">About</a></li>
                        
                        <li><a href="{{ url('/facilities') }}">Facilities</a></li>
                        <li class=""><a href="{{ url('/galery') }}">Galery</a></li>
                        <li class=""><a href="{{ url('/testimony') }}">Testimony</a></li>
                        <li class=""><a href="{{ url('/confirmation/form') }}">Confirmation</a></li>
                    </ul>
                    
                    <div class="search">
                        <form>
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span><input type="email" class="text" value="Enter Your Text..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Your Text...';}" required="">
                        </form>
                    </div>
                </div><!-- /.navbar-collapse -->    
                
            </nav>
        </div>
    </div>
    <!-- blog -->
    <div class="blog">
        <div class="container">
            <h2 class="animated wow zoomIn" data-wow-delay=".5s">Searching Room...</h2>
            <div class="blog-grids">
                <Table class="table table-striped table-hover table-bordered">
                    <tr>
                        <th width="">Room</th>
                        <th width="">Capacity</th>
                        <th width="65%">Service</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                    @foreach($search_room as $search)
                        <tr>
                            <td>{{ $search->room }}</td>
                            <td>{{ $search->capacity }}</td>
                            <td>{{ $search->service }}</td>
                            <td>{{ $search->prices->price }}</td>
                            <td align="center">
                                {!! Form::open(['url' => url('/booking/form'), 'method' => 'post', 'class'=>'form-horizontal']) !!}
                                    <input type="text" name="room_id" value="{{ $search->id }}" hidden="hidden">
                                    <input type="date" name="check_in" value="{{ $check_in }}" hidden="hidden">
                                    <input type="date" name="check_out" value="{{ $check_out }}" hidden="hidden">
                                    <input type="submit" value="Booking" class="btn btn-success">
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </Table>
            </div>
            
        </div>
    </div>
<!-- //blog -->
@endsection
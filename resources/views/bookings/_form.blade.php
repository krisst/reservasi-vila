<h5>Check In</h5>
<div class="book_date">
        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
        <input class="" id="" type="date" name="check_in" required="required">
</div>  
<h5>Check Out</h5>
 <div class="book_date">
        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
        <input class="" id="" type="date" name="check_out" required="required">
</div>
 <h5>Guests</h5>
 
 <div class="section_room">
      <select name="number" class="frm-field required sect" required="required">
            <option value="">--Select--</option>
            <option value="1">1</option>
            <option value="2">2</option> 
            <option value="3">3</option>                 
            <option value="4">4</option>                              
      </select>
 </div> 
<div class="book">
        <input type="submit" value="Make Your Booking">
</div>
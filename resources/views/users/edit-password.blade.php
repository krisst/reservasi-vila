@extends('layouts.app')
@section('content')
	    <section class="content-header">
	      <h1>
	        User
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
	        <li class="active">Change Password</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      	<div class="panel panel-warning">
				<div class="panel-heading">
					<h2 class="panel-title">Change Password</h2>
				</div>
				<div class="panel-body">
					{!! Form::open(['url' => url('admin/settings/change-password'),'method' => 'post', 'class'=>'form-horizontal']) !!}
						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							{!! Form::label('password', 'Old Password', ['class'=>'col-md-3 control-label']) !!}
							<div class="col-md-4">
								{!! Form::password('password', ['class'=>'form-control']) !!}
								{!! $errors->first('password', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
							{!! Form::label('new_password', 'New Password', ['class'=>'col-md-3 control-label']) !!}
							<div class="col-md-4">
								{!! Form::password('new_password', ['class'=>'form-control']) !!}
								{!! $errors->first('new_password', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group{{ $errors->has('new_password_confirmation') ? ' has-error': '' }}">
							{!! Form::label('new_password_confirmation', 'New Confirmation Password', ['class'=>'col-md-3 control-label']) !!}
							<div class="col-md-4">
								{!! Form::password('new_password_confirmation', ['class'=>'form-control']) !!}
								{!! $errors->first('new_password_confirmation', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
								{!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
							</div>
						</div>
						{!! Form::close() !!}	
				</div>
			</div>
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->
@endsection
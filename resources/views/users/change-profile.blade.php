@extends('layouts.app')
@section('content')
	    <section class="content-header">
	      <h1>
	        User
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
	        <li class="active">Change Profile</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      	<div class="panel panel-warning">
				<div class="panel-heading">
					<h2 class="panel-title">Change Profile</h2>
				</div>
				<div class="panel-body">
					{!! Form::open(['url' => url('admin/settings/change-profile'),'method' => 'post', 'class'=>'form-horizontal']) !!}
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							{!! Form::label('name', 'Username', ['class'=>'col-md-2 control-label']) !!}
							<div class="col-md-4">
								{!! Form::text('name', $users->user->name, ['class'=>'form-control']) !!}
								{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							{!! Form::label('email', 'Email', ['class'=>'col-md-2 control-label']) !!}
							<div class="col-md-4">
								{!! Form::email('email', $users->user->email, ['class'=>'form-control']) !!}
								{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-2 col-md-offset-2">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-file"></i> Simpan
								</button>
							</div>
						</div>
					{!! Form::close() !!}	
				</div>
			</div>
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->
@endsection
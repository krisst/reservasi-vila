@extends('layouts.app')

@section('content')
	    <section class="content-header">
	      <h1>
	        Dashboard
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li class="active">Dashboard</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      <div class="col-lg-2 col-md-2 col-xs-2 thumb" align="center">
	                <a class="thumbnail" href="{{ url('/home') }}">
	                    <i class="fa fa-fw fa-dashboard fa-5x"></i>
	                    <h4>Dashboard<br><br></h4>
	                </a>
	            </div>
	            <div class="col-lg-2 col-md-2 col-xs-2 thumb" align="center">
	                <a class="thumbnail" href="{{ route('users.index') }}">
	                    <i class="fa fa-fw fa-user fa-5x"></i>
	                    <h4>Profile<br><br></h4>
	                </a>
	            </div>
	            <div class="col-lg-2 col-md-2 col-xs-2 thumb" align="center">
	                <a class="thumbnail" href="{{ route('rooms.index') }}">
	                    <i class="fa fa-fw fa-columns fa-5x"></i>
	                    <h4>Room<br><br></h4>
	                </a>
	            </div>
	            <div class="col-lg-2 col-md-2 col-xs-2 thumb" align="center">
	                <a class="thumbnail" href="{{ route('list_booking.index') }}">
	                    <i class="fa fa-fw fa-book fa-5x"></i>
	                    <h4>Booking<br><br></h4>
	                </a>
	            </div>
	            <div class="col-lg-2 col-md-2 col-xs-2 thumb" align="center">
	                <a class="thumbnail" href="{{ route('guests.index') }}">
	                    <i class="fa fa-fw fa-users fa-5x"></i>
	                    <h4>Guest<br><br></h4>
	                </a>
	            </div>
	            <div class="col-lg-2 col-md-2 col-xs-2 thumb" align="center">
	                <a class="thumbnail" href="{{ route('confirmations.index') }}">
	                    <i class="fa fa-fw fa-phone fa-5x"></i>
	                    <h4>Confirmation<br><br></h4>
	                </a>
	            </div>
	            <div class="col-lg-2 col-md-2 col-xs-2 thumb" align="center">
	                <a class="thumbnail" href="{{ route('logout') }}" onclick="event.preventDefault();
	                                                     document.getElementById('logout-form').submit();">
	                    <i class="fa fa-fw fa-power-off fa-5x"></i>
	                    <h4>Logout<br><br></h4>
	                </a>
	            </div>
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->

@endsection

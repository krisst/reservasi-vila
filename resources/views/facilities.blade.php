@extends('layouts.home')

@section('content')
    

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('/about') }}">About</a></li>
                        
                        <li class="active"><a href="{{ url('/facilities') }}">Facilities</a></li>
                        <li><a href="{{ url('/galery') }}">Galery</a></li>
                        <li class=""><a href="{{ url('/testimony') }}">Testimony</a></li>
                        <li class=""><a href="{{ url('/confirmation/form') }}">Confirmation</a></li>
                    </ul>
                    
                </div><!-- /.navbar-collapse -->    
                
            </nav>
        </div>
    </div>
    <!-- about -->
    <div class="about">
        <div class="container">
            <div class="about-grids">
                <div class="col-md-6 about-grid animated wow slideInLeft" data-wow-delay=".5s">
                    <h3>Guest Facilities</h3>
                    <div class="about-gd">
                        <div class="about-gd-left">
                            <h4><span class="glyphicon glyphicon-ok-circle"></span></h4>
                        </div>
                        <div class="about-gd-right">
                            <p>Open 24 Hours</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="about-gd">
                        <div class="about-gd-left">
                            <h4><span class="glyphicon glyphicon-ok-circle"></span></h4>
                        </div>
                        <div class="about-gd-right">
                            <p>Lobby</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="about-gd">
                        <div class="about-gd-left">
                            <h4><span class="glyphicon glyphicon-ok-circle"></span></h4>
                        </div>
                        <div class="about-gd-right">
                            <p>Gazebo</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="about-gd">
                        <div class="about-gd-left">
                            <h4><span class="glyphicon glyphicon-ok-circle"></span></h4>
                        </div>
                        <div class="about-gd-right">
                            <p>Complimentary Parking</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="about-gd">
                        <div class="about-gd-left">
                            <h4><span class="glyphicon glyphicon-ok-circle"></span></h4>
                        </div>
                        <div class="about-gd-right">
                            <p>Musholla</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="about-gd">
                        <div class="about-gd-left">
                            <h4><span class="glyphicon glyphicon-ok-circle"></span></h4>
                        </div>
                        <div class="about-gd-right">
                            <p>Cleaning Service</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="about-gd">
                        <div class="about-gd-left">
                            <h4><span class="glyphicon glyphicon-ok-circle"></span></h4>
                        </div>
                        <div class="about-gd-right">
                            <p>Garden</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
<!-- //about -->
@endsection
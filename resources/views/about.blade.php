@extends('layouts.home')

@section('content')
    

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li class="active"><a href="{{ url('/about') }}">About</a></li>
                        
                        <li><a href="{{ url('/facilities') }}">Facilities</a></li>
                        <li><a href="{{ url('/galery') }}">Galery</a></li>
                        <li class=""><a href="{{ url('/testimony') }}">Testimony</a></li>
                        <li class=""><a href="{{ url('/confirmation/form') }}">Confirmation</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->    
                
            </nav>
        </div>
    </div>
    <!-- about -->
    <div class="about">
        <div class="container">
            <div class="about-grids">
                <div class="col-md-6 about-grid animated wow slideInLeft" data-wow-delay=".5s">
                    <h3>About Us</h3>
                    <img src="images/banner2.jpg" alt=" " class="img-responsive" />
                    <h2>Vilaa Jaya Sakti adalah perusahaan yang bergerak di bidang penyewaan villa yang beralamat di Jalan Pelabuhan Rancabuaya Kec. Caringin Kab. Garut.</h2>
                    <p> Villa yang berlokasi didekat pantai ini memiliki 30 kamar yang terdiri dari 2 tipe kamar yaitu tipe yang pertama memiliki fasilitas 1 tempat tidur dan yang kedua memiliki fasilitas 2 tempat tidur.
                    Fasilitas kamar pada Villa Jaya Sakti ini dilengkapi dengan televisi berbayar, AC (Air Conditioner), Water Hater dan perlengkapan mandi yang lengkap. Villa Jaya Sakti ini melayani 24 jam dan tarif yang ditawarkan relatif murah dan bersaing. Lokasi yang strategis dan dekat dengan pantai, serta mempunyai pemandangan yang indah.
</p>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
<!-- //about -->
@endsection
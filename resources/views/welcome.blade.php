@extends('layouts.home')

@section('content')
    

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('/about') }}">About</a></li>
                        
                        <li><a href="{{ url('/facilities') }}">Facilities</a></li>
                        <li><a href="{{ url('/galery') }}">Galery</a></li>
                        <li class=""><a href="{{ url('/testimony') }}">Testimony</a></li>
                        <li class=""><a href="{{ url('/confirmation/form') }}">Confirmation</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->    
                
            </nav>
        </div>
    </div>
    <!-- banner -->
    <div class="banner">
        <div class="banner-pos banner1">
            <div class="container">
            @include('layouts._flash')
                <div class="banner-info animated wow slideInUp" data-wow-delay=".5s">
                    <h2>
                        Welcome To The Jaya Sakti <span>Villa</span>
                    </h2>
                    <p>Amazing And delicious dishes only with Us !</p>
                </div>
            </div>
            <div class="banner-posit animated wow zoomIn" data-wow-delay=".5s">

                <h2><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>Booking A Room</h2>
                <div class="reservation">
                    {!! Form::open(['url' => url('/search/room'), 'method' => 'post', 'class'=>'form-horizontal']) !!}
                        @include('bookings._form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>  
    </div>

                            

<!-- //banner -->
@endsection
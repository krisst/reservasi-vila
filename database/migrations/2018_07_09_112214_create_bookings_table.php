<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('room_id');
            $table->foreign('room_id')->references('id')->on('rooms')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedInteger('price_id');
            $table->foreign('price_id')->references('id')->on('prices')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->date('check_in');
            $table->date('check_out');
            $table->string('status')->length(20);    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropForeign('bookings_room_id_foreign');
        });
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropForeign('bookings_price_id_foreign');
        });
        Schema::dropIfExists('bookings');
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Price;

class PricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $price1 = Price::create(['room_id'=>'1','price'=>'1000000']);
        $price2 = Price::create(['room_id'=>'2','price'=>'2000000']);
        $price3 = Price::create(['room_id'=>'3','price'=>'1000000']);
        $price4 = Price::create(['room_id'=>'4','price'=>'2000000']);
        $price5 = Price::create(['room_id'=>'5','price'=>'3000000']);
        $price6 = Price::create(['room_id'=>'6','price'=>'1000000']);
        $price7 = Price::create(['room_id'=>'7','price'=>'2000000']);
        $price8 = Price::create(['room_id'=>'8','price'=>'1000000']);
        $price9 = Price::create(['room_id'=>'9','price'=>'750000']);
        $price10 = Price::create(['room_id'=>'10','price'=>'750000']);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Room;

class RoomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $room1 = Room::create(['room'=>'R001','capacity'=>'2','service'=>'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel']);
        $room2= Room::create(['room'=>'R002','capacity'=>'4','service'=>'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel']);
        $room3 = Room::create(['room'=>'R003','capacity'=>'2','service'=>'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel']);
        $room4 = Room::create(['room'=>'R004','capacity'=>'4','service'=>'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel']);
        $room5 = Room::create(['room'=>'R005','capacity'=>'6','service'=>'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel']);
        $room6 = Room::create(['room'=>'R006','capacity'=>'2','service'=>'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel']);
        $room7 = Room::create(['room'=>'R007','capacity'=>'4','service'=>'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel']);
        $room8 = Room::create(['room'=>'R008','capacity'=>'2','service'=>'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel']);
        $room9 = Room::create(['room'=>'R009','capacity'=>'1','service'=>'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel']);
        $room10 = Room::create(['room'=>'R010','capacity'=>'1','service'=>'Ruangan bebas rokok, Memiliki balkon/teras pribadi, Dilengkapi oleh fasilitas lemari, Ac, telepon, wifi, brankas, Tv lokal/kabel, Pembuat minuman kopi/teh, lemari es, meja kerja, dan sofa, Kamar mandi shower dengan air panas dan dingin beserta perlengkapannya, Tersedia sandal hotel']);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Booking;

class BookingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $booking1 = Booking::create(['room_id'=>'1','price_id'=>'1','check_in'=>'2018-07-24','check_out'=>'2018-07-27','status'=>'Belum Transfer','customer_id'=>'1']);
        $booking2 = Booking::create(['room_id'=>'2','price_id'=>'2','check_in'=>'2018-07-14','check_out'=>'2018-07-15','status'=>'Belum Transfer','customer_id'=>'2']);
        $booking3 = Booking::create(['room_id'=>'3','price_id'=>'3','check_in'=>'2018-07-24','check_out'=>'2018-07-26','status'=>'Belum Transfer','customer_id'=>'3']);
        $booking4 = Booking::create(['room_id'=>'4','price_id'=>'4','check_in'=>'2018-07-22','check_out'=>'2018-07-24','status'=>'Belum Transfer','customer_id'=>'4']);
        $booking5 = Booking::create(['room_id'=>'5','price_id'=>'5','check_in'=>'2017-07-22','check_out'=>'2017-07-24','status'=>'Belum Transfer','customer_id'=>'4']);
        $booking6 = Booking::create(['room_id'=>'6','price_id'=>'6','check_in'=>'2017-07-22','check_out'=>'2017-07-24','status'=>'Belum Transfer','customer_id'=>'4']);
        $booking7 = Booking::create(['room_id'=>'7','price_id'=>'7','check_in'=>'2017-07-22','check_out'=>'2017-07-24','status'=>'Belum Transfer','customer_id'=>'4']);
        $booking8 = Booking::create(['room_id'=>'8','price_id'=>'8','check_in'=>'2017-07-22','check_out'=>'2017-07-24','status'=>'Belum Transfer','customer_id'=>'4']);
        $booking9 = Booking::create(['room_id'=>'9','price_id'=>'9','check_in'=>'2017-07-22','check_out'=>'2017-07-24','status'=>'Belum Transfer','customer_id'=>'4']);
        $booking10 = Booking::create(['room_id'=>'10','price_id'=>'10','check_in'=>'2017-07-22','check_out'=>'2017-07-24','status'=>'Belum Transfer','customer_id'=>'4']);
    }
}
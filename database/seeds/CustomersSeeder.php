<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $customer1 = Customer::create(['name'=>'Tinny','email'=>'tinny@gmail.com','phone_number'=>'847383747','no_rek'=>'12443223','bank'=>'BCA','atas_nama'=>'Tinny']);
        $customer2 = Customer::create(['name'=>'Denny','email'=>'denny@gmail.com','phone_number'=>'847383741','no_rek'=>'12443222','bank'=>'BCA','atas_nama'=>'Denny']);
        $customer3 = Customer::create(['name'=>'Donny','email'=>'donny@gmail.com','phone_number'=>'847383743','no_rek'=>'12443227','bank'=>'BRI','atas_nama'=>'Donny']);
        $customer4 = Customer::create(['name'=>'Winny','email'=>'winny@gmail.com','phone_number'=>'847383742','no_rek'=>'12443229','bank'=>'BNI','atas_nama'=>'Winny']);
    }
}
